﻿namespace WebApiAutores.DTO
{
    public class AutorDTO
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
    }
}
