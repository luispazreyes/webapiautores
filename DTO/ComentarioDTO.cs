﻿namespace WebApiAutores.DTO
{
    public class ComentarioDTO
    {
        public string Id { get; set; }
        public string Contenido { get; set; }
    }
}
